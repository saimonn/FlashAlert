# FlashAlert

Cette application permet d'avoir une notification visuelle lors de la réception d'un appel ou un message SMS/MMS.
Cette notification est réalisée grace au flash de l'appareil photo, si le device en est équipé.


# Options
<dl>
  <dt>Détection sur le type d'évenement</dt>
  <dd>Sur la réception d'un appel téléphonique.</dd>
  <dd>Sur la réception d'un SMS ou MMS.</dd>
  <dd>Sur une notification applicative.</dd>

  <dt>Détéction sur le type de contact</dt>
  <dd>Notification pour tous les contacts.</dd>
  <dd>Notification sur les contacts du téléphone.</dd>
  
  <dt>Notification si l'écran est éteint</dt>
  
  <dt>Gestion du mode ne pas déranger ( DND Mode )</dt>
  
  <dt>Mode nuit </dt>
</dl>

# Status de compilation 

Etat de la dernière compilation : [![build status](https://gitlab.com/jnda/FlashAlert/badges/master/build.svg)](https://gitlab.com/jnda/FlashAlert/builds)


# Téléchargement

Version FDroid
